# Writing Artifacts

Writing artifacts, such as data, workbooks, images, etc.

## Index

1. covid/nh_all_cause_mortality: Examining NH all-cause mortality. Published on 12/18/20 at 
      [Granite Grok](https://granitegrok.com/mg_gilfordgrok/2020/12/covid-in-new-hampshire-what-emergency).
