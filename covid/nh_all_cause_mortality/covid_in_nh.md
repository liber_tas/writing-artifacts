# Covid in New Hampshire: What Emergency?

According to the media and the New Hampshire Governor and Department of Health (DHHS) we are experiencing a 
once-in-a-lifetime event in NH, with an outsize toll in human lives. They’ve argued for, and implemented, draconian 
and destructive measures, under “emergency” provisions of the law. The economic costs of these measures will be borne 
for decades to come, most keenly felt by the most vulnerable in our State.  

But data increasingly shows that the response in NH was unnecessary, and that this continued economic destruction 
against Granite Staters is now being wrought for no reason. 

## Perception and Reality: Sweden

To set the context, consider a country that did not implement emergency measures: Sweden. Here is a graph showing 
the all-cause mortality rate in Sweden, screenshot from a video by the very methodical and even-keeled 
[Ivor Cummins](https://www.youtube.com/watch?v=8UvFhIFzaac&t=1261s) (if you like data, you’ll love him).

![Sweden all-cause mortality](https://gitlab.com/liber_tas/writing-artifacts/-/blob/master/covid/nh_all_cause_mortality/img/sweden.png)
_Graph 1: Sweden All-cause Mortality_

We can see that ten years in the last thirty (fully one third) was roughly equivalent to 2020. And three other 
years during that period exceed 2020 -- by up to 30%. And, 2020 is dwarfed by the Spanish Flu, a real 
once-in-a-lifetime event. From this data, it is impossible to claim that any disaster struck Sweden in 2020, 
nevermind “unprecedented”. Can we really argue that Sweden was worse off in 2020 than in 1994, when roughly 30% 
more died? And does anyone remember 1994 as a disaster for Sweden?

## New Hampshire All Cause Mortality

Turning to the numbers for NH, the lack of an “emergency” is stark. The State’s 
[NH Vital Records website](https://nhvrinweb.sos.nh.gov/) provides current mortality data from January 2005 to 
present. With population data from [Macrotrends](https://www.macrotrends.net/states/new-hampshire/population), 
we can graph the monthly mortality rate per capita for NH.

![NH All-cause Mortality by Month](https://gitlab.com/liber_tas/writing-artifacts/-/blob/master/covid/nh_all_cause_mortality/img/mortality_by_month.png)
_Graph 2: NH All-cause Mortality by Month_

In the graph, we notice a strong seasonal cycle. Instead of using calendar years for analysis, it makes more sense 
to consider years centered on February 1 (peak flu season), starting on August 1, and ending on July 30th. 
This “Shifted Year” neatly captures a full flu season, and also this Spring’s Covid death curve. Back to the 
graph, it is also clear that there is a distinct linear increase during the last decade, and that the peak in 
2020 does not seem to be out of the ordinary. 

## Reality for NH

When we calculate the annual “Shifted Years” mortality rates , the same linear upward trend over the last decade 
can be seen, starting in 2008 or 2009, in Graph 3 below. 

![NH Annual Mortality Rate](https://gitlab.com/liber_tas/writing-artifacts/-/blob/master/covid/nh_all_cause_mortality/img/mortality_by_year.png)
_Graph 3: NH Annual Mortality Rate_

Fitting a line (in red) to the 2008/09-2019/20 data, we can see that 2019/2020 is well within the bounds of what 
could have been expected of a normal year. It is worth noting that 2009/10 and 2013/14 deviates more from the 
trend line than 2019/20 does. No disaster visible, whatsoever.

We can also look at the percentage change in mortality year-on-year -- in Graph 4 below.

![200~Mortality Rate Changes](https://gitlab.com/liber_tas/writing-artifacts/-/blob/master/covid/nh_all_cause_mortality/img/mortality_change.png)
_Graph 4: Mortality Rate Changes_

What is striking is that 2011 and 2015 had a noticeably larger percentage change in mortality rates than 2020. 
The question is, if 2020 was a “disaster” with emergency measures due to a rise in mortality, was 2017, 2015 and 
2011 not also disasters? Or, is it more accurate to say 2020 was not a disaster either?

## What is the Government Doing?

All of this is not to argue that Covid did not have a measurable impact in other places, or that individual deaths 
do not matter, just that Covid shows no “emergency” at the New Hampshire State level. Any policy at the State 
level has to be based on conditions in that State, not on conditions elsewhere. Lock-downs and mask mandates for 
its own residents make very little sense if the impact is as low as it has been in NH.

Let’s assume, for argument’s sake, that lockdowns were an understandable reaction (even if in error) in early 
Spring as a precaution against a disease with an unknown impact. Now that we know that there was nothing like the 
predicted disaster, why do we still have the destructive precautions that weren’t needed in the first place? We have 
the data now. We know that the impact of the disease on the overall mortality rate of NH has been negligible, even 
statistically undetectable. We know that future impact should be similarly small. We know there was no need for 
an emergency imposition. Why then, is the Governor and the DHHS telling us there was a disaster, and that it is 
on-going? Why are the Governor and DHHS making things worse, rather than better?

